@extends('master')
@section('content')
{{ $status or ' ' }}
<div class="panel panel-info">
	<div class="panel-heading">
		Data Admin
		<div class="pull-right">
			<a href="{{ url('admin/tambah')}}" class="btn btn-success btn-sm"></img>Tambah Data</a>
		</div>
	</div>
	<div class="panel-body">
		<table class="table">
				<tr>
					<td>Nama</td>
					<td>No Telepon</td>
					<td>Email</td>
					<td>Alamat</td>
					<td>Id Pengguna</td>
					<td>Aksi</td>
				</tr>
				@foreach($admin as $admin)
					
				<tr>
					<td >{{ $admin->nama}}</td>
					<td >{{ $admin->no_telp}}</td>
					<td >{{ $admin->email}}</td>
					<td >{{ $admin->alamat}}</td>
					<td >{{ $admin->pengguna_id}}</td>

					<td >
					
					<a href="{{url('admin/edit/'.$admin->id)}}" class="btn btn-primary btn-xs">Edit</a>
					<a href="{{url('admin/hapus/'.$admin->id)}}" class="btn btn-danger btn-xs">Hapus</a>
					</td>
				</tr>
				@endforeach
			</table>
	</div>
</div>
@endsection