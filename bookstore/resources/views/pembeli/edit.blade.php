 	@extends('master')
@section('content')

	<div class="panel panel-primary">
		
		{!! Form::model($pembeli,['url'=>'pembeli/update/'.$pembeli->id,'class'=>'form-horizontal']) !!}
		@include('pembeli.form')

		<div style="width:100%;text-align:center;">
			<button class="btn btn-primary"><i class="fa fa-save"></i>
			Simpan</button>
			<input type="button" value="Reset" class="btn btn-danger" onClick="window.location.reload()"/>
		</div>
	{!! Form::close() !!}
	</div>
@endsection