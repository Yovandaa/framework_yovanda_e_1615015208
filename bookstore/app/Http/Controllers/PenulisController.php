<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Penulis;
use App\Http\Requests;
class PenulisController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $penulis=Penulis::all();
        return view ('penulis.app',compact('penulis'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('penulis.tambah');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $input)
    {
        $penulis = new Penulis();
        $penulis->nama=$input->nama;
        $penulis->notelp=$input->notelp;
        $penulis->email=$input->email;
        $penulis->alamat=$input->alamat;
        $status = $penulis->save();
        return redirect('penulis')->with(['status'=>$status]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $penulis=Penulis::find($id);
        return view ('penulis.edit')->with(array('penulis'=>$penulis));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($id, Request $input)
    {
        $penulis=Penulis::find($id);
        $penulis->nama=$input->nama;
        $penulis->notelp=$input->notelp;
        $penulis->email=$input->email;
        $penulis->alamat=$input->alamat;
        $status = $penulis->save();
        return redirect('penulis')->with(['status'=>$status]);   
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $penulis=Penulis::find($id);
        $penulis->delete();
        return redirect('penulis');
    }
}
