<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/','AuthController@index');
Route::get('/login','AuthController@formLogin');
Route::post('/login','AuthController@proses');
Route::get('/logout','AuthController@logout');

Route::get('/awal', function () {
    return view('welcome');
});

Route::get('/tes', function () {
    return view('master');
});

Route::get('/pengguna','PenggunaController@awal');
Route::get('/save','PenggunaController@simpan');

Route::get('/buku','BukuControllerk@index');
Route::get('/tambah','BukuControllerk@show');

Route::get('/tambah/penulis','PenulisControllerk@create');
Route::post('/penulis/simpan','PenulisControllerk@store');
Route::get('/penulis','PenulisControllerk@index');
Route::get('/penulis/edit/{penulis}','PenulisControllerk@edit');
Route::post('/penulis/update/{penulis}','PenulisControllerk@update');
Route::get('/penulis/hapus/{penulis}','PenulisControllerk@destroy');

Route::get('/Buku/tambah','BukuControllerk@create');
Route::post('/buku/simpan','BukuControllerk@store');
Route::get('/Buku/edit/{Buku}','BukuControllerk@edit');
Route::post('/buku/update/{Buku}','BukuControllerk@update');
Route::get('/Buku/hapus/{Buku}','BukuControllerk@destroy');

Route::get('/kategori/tambah','KategoriControllerk@create');
Route::post('/kategori/simpan','KategoriControllerk@store');
Route::get('/kategori/edit/{simpan}','KategoriControllerk@edit');
Route::post('/kategori/update/{simpan}','KategoriControllerk@update');
Route::get('/kategori/hapus/{simpan}','KategoriControllerk@destroy');
Route::get('/kategori','KategoriControllerk@index');

Route::get('/pembeli/tambah','PembeliControllerk@create');
Route::post('/pembeli/simpan','PembeliControllerk@store');
Route::get('/pembeli','PembeliControllerk@index');
Route::get('/pembeli/edit/{pembeli}','PembeliControllerk@edit');
Route::post('/pembeli/update/{pembeli}','PembeliControllerk@update');
Route::get('/pembeli/hapus/{pembeli}','PembeliControllerk@destroy');

Route::get('/admin','AdminControllerk@index');
Route::get('/admin/tambah','AdminControllerk@create');
Route::post('/admin/simpan','AdminControllerk@store');
Route::get('/admin/lihat','AdminControllerk@show');
Route::get('/admin/edit/{admin}','AdminControllerk@edit');
Route::post('/admin/update/{admin}','AdminControllerk@update');
Route::get('/admin/hapus/{admin}','AdminControllerk@destroy');



