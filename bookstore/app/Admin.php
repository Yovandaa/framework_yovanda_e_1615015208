<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Pengguna;

class Admin extends Model
{
    protected $table = 'admin';
    protected $fillable=['nama','no_telp','email','alamat','pengguna_id'];

    public function Pengguna()
    {
    	return $this->belongsTo(Pengguna::class);
    }

    

}
